﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PKBNew.Startup))]
namespace PKBNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
